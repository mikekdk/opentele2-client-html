(function() {
    'use strict';

    var utils = require("./utils.js");

    exports.list = function(req, res, baseUrl) {

        var userToken = utils.decodeAuthorizationHeader(req);
        console.log("Links categories list requested from: " + userToken);

        if (userToken.indexOf("rene") > -1) {
            res.send({
                "categories": [
                    {
                        "name": "Information",
                        "links": {
                            "linksCategory": baseUrl + "patient/links_categories/1"
                        }
                    }
                ],
                "links": {
                    "self": baseUrl + "patient/links_categories"
                }
            });
            return;
        }

        console.log("Links categories returned.");
        res.send(linksCategoriesList(baseUrl));
    };

    exports.get = function(req, res, baseUrl) {

        var linksCategoriesId = req.params.id;
        console.log("Links categories id " + linksCategoriesId);
        var responses = {
            "1": function (baseUrl) {
                res.send(informationCategory(baseUrl));
            },
            "2": function (baseUrl) {
                res.send(videoInstructionsCategory(baseUrl));
            }
        };
        if (responses.hasOwnProperty(linksCategoriesId)) {
            responses[linksCategoriesId](baseUrl);
        } else {
            res.status(404).end();
        }
    };

    var linksCategoriesList = function (baseUrl) {
        return {
            "categories": [
                {
                    "name": "Information",
                    "links": {
                        "linksCategory": baseUrl + "patient/links_categories/1"
                    }
                },
                {
                    "name": "Video instructions",
                    "links": {
                        "linksCategory": baseUrl + "patient/links_categories/2"
                    }
                }
            ],
            "links": {
                "self": baseUrl + "patient/links_categories"
            }
        };
    };

    var informationCategory = function (baseUrl) {
        return {
            "name": "Information",
            "categoryLinks": [
                {
                    "title": "OpenTele user manual",
                    "url": "http://opentele.silverbullet.dk/applications"
                },
                {
                    "title": "Saturation measurement guide",
                    "url": "http://opentele.silverbullet.dk/architecture"
                }
            ],
            "links": {
                "self": baseUrl + "patient/links_categories/1"
            }
        };
    };

    var videoInstructionsCategory = function (baseUrl) {
        return {
            "name": "Video instructions",
            "categoryLinks": [
                {
                    "title": "Demonstration of saturation measurement",
                    "url": "http://opentele.silverbullet.dk/devices"
                }
            ],
            "links": {
                "self": baseUrl + "patient/links_categories/2"
            }
        };
    };

}());
