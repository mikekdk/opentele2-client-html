(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing saturation without pulse device node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var questionnairePage = require('../questionnairePage.js');
        var ioNodePage = require('./ioNodePage.js');
        var saturationWithoutPulseDeviceNodePage = require('./saturationWithoutPulseDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
            measurementsPage.toQuestionnaire("Saturation u/puls", "0.1");
        });

        it('should show questionnaire', function () {
            ioNodePage.headings.then(function(items) {
                expect(items.length).toBe(1);
                expect(items[0].getText()).toMatch(/Saturation/);
            });
        });

        it('should navigate to saturation device node', function () {
            questionnairePage.clickCenterButton();
            expect(saturationWithoutPulseDeviceNodePage.heading.getText()).toMatch(/Saturation/);
        });

        it('should see all info texts and fill out form', function(done) {
            questionnairePage.clickCenterButton();
            expect(saturationWithoutPulseDeviceNodePage.heading.getText()).toMatch(/Saturation/);

            var infoDeferred = protractor.promise.defer();
            var saturationDeferred = protractor.promise.defer();
            setTimeout(function() {
                saturationWithoutPulseDeviceNodePage.info.getText()
                    .then(function(text) {
                        infoDeferred.fulfill(text);
                    });
                saturationWithoutPulseDeviceNodePage.saturation.getAttribute('value')
                    .then(function(value) {
                        saturationDeferred.fulfill(value);
                        done();
                    });
            }, 3500);
            expect(saturationDeferred).toEqual('98');
            expect(infoDeferred).toMatch(/Waiting for measurement/);

        });
    });

}());
