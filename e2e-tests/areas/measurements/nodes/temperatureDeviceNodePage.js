(function() {
    'use strict';

    var TemperatureDeviceNodePage = function() {
        this.heading = element(by.css('h2'));
        this.info = element(by.css('h4'));
        this.error = element(by.binding('nodeModel.error'));
        this.temperature = element(by.id('temperature-measurement'));
    };

    module.exports = new TemperatureDeviceNodePage();

}());
