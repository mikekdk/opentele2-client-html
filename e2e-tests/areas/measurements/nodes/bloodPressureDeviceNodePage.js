(function() {
    'use strict';

    var BloodPressureDeviceNodePage = function() {
        this.heading = element(by.binding('nodeModel.heading'));
        this.info = element(by.binding('nodeModel.info'));
        this.error = element(by.binding('nodeModel.error'));
        this.systolic = element(by.model('nodeModel.systolic'));
        this.diastolic = element(by.model('nodeModel.diastolic'));
        this.pulse = element(by.model('nodeModel.pulse'));
    };

    module.exports = new BloodPressureDeviceNodePage();

}());
