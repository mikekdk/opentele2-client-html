(function() {
    'use strict';

    var DecisionNodePage = function() {
		var questionnairePage = require('../questionnairePage.js');

        this.enterSystolicValue = function(value) {
            element.all(by.tagName('input')).get(0).sendKeys(value);
        };

		this.enterDiastolicValue = function(value) {
            element.all(by.tagName('input')).get(1).sendKeys(value);
		};

		this.enterPulseValue = function(value) {
            element.all(by.tagName('input')).get(2).sendKeys(value);
		};

		this.clickYes = function() {
			questionnairePage.clickRightButton();
		};

		this.clickNo = function() {
			questionnairePage.clickLeftButton();
		};
    };

    module.exports = new DecisionNodePage();
}());
