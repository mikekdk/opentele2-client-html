(function() {
    'use strict';

    var LinksCategoriesPage = function () {
        this.menuButton = element(by.id('menu-button'));
        this.title = element(by.css('.title'));
        this.linksCategoriesList = element(by.id('links-categories-list'));
        this.linksCategoriesNames = element.all(by.binding('linksCategoryRef'));

        this.get = function () {
            browser.get('index.html#/links_categories');
        };

        this.toLinksCategory = function (index) {
            element.all(by.id('links-categories-items')).
                get(index).
                $('a').
                click();
        };
    };

    module.exports = new LinksCategoriesPage();

}());
