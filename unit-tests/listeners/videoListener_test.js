(function () {
    'use strict';

    describe('opentele.listeners.video', function() {
        var templateCache, location, timeout, nativeService, videoService,
            saturationListener, bloodPressureListener, videoListener, nativeEventCallback;
        var url = "http://www.example.com";
        var pendingMeasurement = {
            type: 'SATURATION'
        };

        beforeEach(module('opentele.listeners'));

        beforeEach(module(function($provide) {

            var nativeService = {
                addDeviceListener: function(measurementType)  {},
                subscribeToMultipleMessages: function(messageType, callback) {
                    nativeEventCallback = callback;
                },
                unsubscribeAll: function(messageType) {},
                removeDeviceListeners: function() {}
            };
            $provide.value('nativeService', nativeService);

            var videoService = {
                pollForPendingMeasurement: function(url, onSuccess) {
                    onSuccess(pendingMeasurement);
                }
            };
            $provide.value('videoService', videoService);

            var saturationListener = {
                create: function(model) {
                    model.saturation = 98;
                    model.pulse = 70;
                    return function(event) {
                        // TODO
                    };
                }
            };
            $provide.value('saturationListener', saturationListener);

            var bloodPressureListener = {
                create: function() {
                    return function(event) {
                        // TODO
                    };
                }
            };
            $provide.value('bloodPressureListener', bloodPressureListener);

        }));

        beforeEach(inject(function($templateCache, $location, $timeout, _nativeService_,
                                   _videoService_, _saturationListener_,
                                   _bloodPressureListener_, _videoListener_) {
            location = $location;
            timeout = $timeout;
            nativeService = _nativeService_;
            videoService = _videoService_;
            saturationListener = _saturationListener_;
            bloodPressureListener = _bloodPressureListener_;
            videoListener = _videoListener_;
        }));

        it('should parse conference ended event', inject(function($timeout) {
            var conferenceEndedEvent = {
                "timestamp": "2015-05-11T12:33:13.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "conference ended"
                }
            };
            var model = {};

            videoListener.setup(url, model);

            nativeEventCallback({
                messageType: "videoConferenceResponse",
                event: conferenceEndedEvent
            });

            $timeout.flush();
            expect(location.path()).toEqual("/menu");
        }));

        it('should parse conference error event', inject(function($timeout) {
            var conferenceErrorEvent = {
                "timestamp": "2015-05-11T12:33:13.000+02:00",
                "type": "status",
                "status": {
                    "type": "error",
                    "message": "conference error"
                }
            };
            var model = {};

            videoListener.setup(url, model);

            nativeEventCallback({messageType: "videoConferenceResponse",event: conferenceErrorEvent});

            $timeout.flush();
            expect(location.path()).toEqual("/menu");
        }));

    });
}());
