(function() {
    'use strict';

    var fakeNativeLayer = angular.module('opentele.fakeNativeLayer', []);

    fakeNativeLayer.run(function($window, $log) {

        var patientPrivacyPolicyText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
                "seddo eiusmod tempor incididunt ut labore et dolore magna " +
                "aliqua. Utenim ad minim veniam, quis nostrud exercitation " +
                "ullamco laboris nisiut aliquip ex ea commodo consequat. " +
                "Duis aute irure dolor in reprehenderit in voluptate velit" +
                "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint" +
                "occaecat cupidatat non proident, sunt in culpa qui officia" +
                "deserunt mollit anim id est laborum.";


        var weightEvents = [

            {
                "timestamp": "2015-05-11T12:33:03.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTING"
                }
            },

            {
                "timestamp": "2015-05-11T12:33:05.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTED"
                }
            },

            {
                "timestamp": "2015-05-11T12:33:07.000+02:00",
                "device": {
                    "systemId": "42",
                    "serialNumber": "1234-555",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "5-t",
                    "eui64": "1234567890",
                    "model": "ABC-1234"
                },
                "type": "measurement",
                "measurement": {
                    "type": "weight",
                    "unit": "kg",
                    "value": 85.3
                }
            }

        ];

        var temperatureEvents = [

            {
                "timestamp": "2015-05-11T12:33:03.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTING"
                }
            },

            {
                "timestamp": "2015-05-11T12:33:05.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTED"
                }
            },

            {
                "timestamp": "2015-05-11T12:33:07.000+02:00",
                "device": {
                    "systemId": "42",
                    "serialNumber": "1234-555",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "5-t",
                    "eui64": "1234567890",
                    "model": "ABC-1234"
                },
                "type": "measurement",
                "measurement": {
                    "type": "temperature",
                    "unit": "C",
                    "value": 37.3
                }
            }

        ];

        var bloodPressureEvents = [

            {
                "timestamp": "2015-05-11T12:31:13.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTING"
                }
            },

            {
                "timestamp": "2015-05-11T12:31:15.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "CONNECTED"
                }
            },

            {
                "timestamp": "2015-05-11T12:31:17.000+02:00",
                "device": {
                    "systemId": "22",
                    "serialNumber": "1234-777",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "5-t",
                    "eui64": "1234567890",
                    "model": "ABC-3456"
                },
                "type": "measurement",
                "measurement": {
                    "type": "blood pressure",
                    "unit": "mmHg",
                    "value": {
                        "systolic": 122,
                        "diastolic": 95,
                        "meanArterialPressure": 108
                    }
                }
            },

            {
                "timestamp": "2015-05-11T12:31:17.000+02:00",
                "device": {
                    "systemId": "22",
                    "serialNumber": "1234-777",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "5-t",
                    "eui64": "1234567890",
                    "model": "ABC-3456"
                },
                "type": "measurement",
                "measurement": {
                    "type": "pulse",
                    "unit": "bpm",
                    "value": 80
                }
            }
        ];

        var saturationEvents = [

            {
                "timestamp": "2015-06-09T13:43:13.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "SATURATION_CONNECTING"
                }
            },

            {
                "timestamp": "2015-06-09T13:43:15.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "SATURATION_CONNECTED"
                }
            },

            {
                "timestamp": "2015-06-09T13:43:17.000+02:00",
                "device": {
                    "systemId": "37",
                    "serialNumber": "4332-321",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "4-2",
                    "eui64": "01234543210",
                    "model": "ABC-7890"
                },
                "type": "measurement",
                "measurement": {
                    "type": "saturation",
                    "unit": "%",
                    "value": 98
                }
            },

            {
                "timestamp": "2015-06-09T13:43:17.000+02:00",
                "device": {
                    "systemId": "37",
                    "serialNumber": "4332-321",
                    "manufacturer": "ACME Inc",
                    "firmwareRevision": "4-2",
                    "eui64": "01234543210",
                    "model": "ABC-7890"
                },
                "type": "measurement",
                "measurement": {
                    "type": "pulse",
                    "unit": "bpm",
                    "value": 80
                }
            },

            {
                "timestamp": "2015-06-09T13:43:19.000+02:00",
                "type": "status",
                "status": {
                    "type": "info",
                    "message": "SATURATION_DISCONNECTED"
                }
            }

        ];

        var joinConference = function(message) {
            var events = [
                {
                    "timestamp": "2015-05-11T12:33:03.000+02:00",
                    "type": "status",
                    "status": {
                        "type": "info",
                        "message": "conference started"
                    }
                },
                {
                    "timestamp": "2015-05-11T12:33:13.000+02:00",
                    "type": "status",
                    "status": {
                        "type": "info",
                        "message": "conference ended"
                    }
                }
            ];

            var i = 0;
            setTimeout(function() {
                var event = events[0];
                sendMessageToWebView({messageType: "videoConferenceResponse", event: event});
            }, 1000);
            setTimeout(function() {
                var event = events[1];
                sendMessageToWebView({messageType: "videoConferenceResponse", event: event});
            }, 11000);
        };

        var addDeviceListener = function(measurementType) {
            var events = [];
            switch (measurementType) {
            case "weight":
                events = weightEvents;
                break;
            case "temperature":
                events = temperatureEvents;
                break;
            case "blood pressure":
                events = bloodPressureEvents;
                break;
            case "saturation":
                events = saturationEvents;
                break;
            case "lung function":
                events = lungFunctionEvents;
                break;
            default:
                console.log("Unknown measurement type: " + measurementType);
                return;
            }

            var i = 0;
            var eventInterval = setInterval(function() {
                if (i >= events.length) {
                    clearInterval(eventInterval);
                    return;
                }

                var event = events[i];
                i++;
                sendMessageToWebView({messageType: "deviceMeasurementResponse", measurementType: measurementType, event: event});
            }, 1000);
        };

        var sendMessageToWebView = function(message) {
            $window.sendMessageToWebView(JSON.stringify(message));
        };

        $window.sendMessageToNative = function(message) {
            $log.info("Message received by fake native layer: " + message.messageType);
            setTimeout(function() {
                switch (message.messageType) {
                    case "patientPrivacyPolicyRequest":
                        sendMessageToWebView({messageType: "patientPrivacyPolicyResponse", text: patientPrivacyPolicyText});
                        break;
                    case "deviceInformationRequest":
                        sendMessageToWebView({messageType: "deviceInformationResponse"});
                        break;
                    case "openUrlRequest":
                        $window.location.href = message.url;
                        break;
                    case "deviceMeasurementRequest":
                        addDeviceListener(message.measurementType);
                        break;
                    case "stopDeviceMeasurementRequest":
                        break;
                    case "videoEnabledRequest":
                        sendMessageToWebView({messageType: "videoEnabledResponse", enabled: false}); // set 'enabled' to true to mock the video conference feature
                        break;
                    case "startNotificationSoundRequest":
                        break;
                    case "stopNotificationSoundRequest":
                        break;
                    case "startVideoConferenceRequest":
                        joinConference(message);
                        break;
                    case "overdueQuestionnairesRequest":
                        sendMessageToWebView({messageType: "overdueQuestionnairesResponse", questionnaireNames: ["Blodsukker (manuel)"]});
                        break;
                    case "setupRemindersRequest":
                        console.log("Reminders: " + JSON.stringify(message.reminders));
                        break;
                    case "clearQuestionnaireReminderRequest":
                        console.log("Questionnaire name to clear reminder for: " + message.questionnaireName);
                        break;
                    default:
                        console.log("Fake native layer does not support message of type: " + message.type);
                }
            }, 100);
        };
    });

}());
