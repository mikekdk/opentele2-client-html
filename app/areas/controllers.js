(function() {
    'use strict';

    var controllers = angular.module('opentele.controllers', [
        'opentele.controllers.acknowledgements',
        'opentele.controllers.categoryLinks',
        'opentele.controllers.changePassword',
        'opentele.controllers.joinConference',
        'opentele.controllers.errors',
        'opentele.controllers.header',
        'opentele.controllers.linksCategories',
        'opentele.controllers.login',
        'opentele.controllers.measurement',
        'opentele.controllers.measurements',
        'opentele.controllers.menu',
        'opentele.controllers.messages',
        'opentele.controllers.myMeasurements',
        'opentele.controllers.newMessage',
        'opentele.controllers.questionnaire',
        'opentele.controllers.sendReply',
        'opentele.controllers.thread'
    ]);

}());
