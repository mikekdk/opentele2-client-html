(function () {
    'use strict';

    var linksCategoriesService = angular.module('opentele.restApiServices.linksCategories', [
        'ngResource'
    ]);

    linksCategoriesService.service('linksCategories', function ($http) {

        var wrapSuccess = function (onSuccess) {
            return function (responseData) {
                if (typeof(responseData) === 'undefined' || responseData === null) {
                    onSuccess({});
                    return;
                }
                delete responseData.links;
                onSuccess(responseData);
            };
        };

        var listFor = function (user, onSuccess) {
            if (!user.hasOwnProperty('links') || !user.links.hasOwnProperty('linksCategories')) {
                throw new TypeError('User object does not contain a link relation to my links categories');
            }

            var url = user.links.linksCategories;
            $http.get(url).success(wrapSuccess(onSuccess));
        };

        var get = function (linksCategoryRef, onSuccess) {
            if (!linksCategoryRef.hasOwnProperty('links') ||
                !linksCategoryRef.links.hasOwnProperty('linksCategory')) {
                throw new TypeError('Links category ref does not contain a link relation to category links');
            }

            var url = linksCategoryRef.links.linksCategory;
            $http.get(url).success(wrapSuccess(onSuccess));
        };

        return {
            listFor: listFor,
            get: get
        };

    });
}());
