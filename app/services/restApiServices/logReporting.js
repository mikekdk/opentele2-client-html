(function () {
    'use strict';

    var logReportingService = angular.module('opentele.restApiServices.logReporting', [
        'ngResource',
        'opentele.stateServices',
        'opentele-commons.nativeServices'
    ]);

    logReportingService.service('logReportingService', function ($http, restConfig,
                                                          appConfig, nativeService) {

        var generateErrorMessage = function(exception) {
            return {
                //Insert desired log information here
            };
        };

        var log = function(exception, onSuccess, onError) {

            nativeService.getDeviceInformation(function(deviceInfo) {
                var postUrl = appConfig.loggingUrl;
                var config = {errorPassThrough: true};
                var errorMessage = generateErrorMessage(exception, deviceInfo);

                var wrapResponse = function(response) {
                    return function() {
                        if (response !== undefined && response !== null) {
                            response();
                        }
                    };
                };

                $http.post(postUrl, errorMessage, config).
                    success(wrapResponse(onSuccess)).
                    error(wrapResponse(onError));
            });
        };

        return {
            log: log
        };

    });

}());
