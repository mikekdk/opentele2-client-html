(function() {
    'use strict';

    var videoListener = angular.module('opentele.listeners.video', [
        'opentele.restApiServices',
        'opentele.listeners',
        'opentele-commons.deviceListeners',
        'opentele-commons.nativeServices'
    ]);

    videoListener.service('videoListener', function($templateCache, $rootScope,
                                                    $location,
                                                    $timeout,
                                                    nativeService,
                                                    videoService,
                                                    saturationListener,
                                                    bloodPressureListener) {

        var constants = {
            STATUS: 'status',
            INFO: 'info',
            ERROR: 'error'
        };

        var CONFERENCE_STARTED = "conference started";
        var CONFERENCE_ENDED = "conference ended";
        var CONFERENCE_ERROR = "conference error";
        var CONFERENCE_MEASUREMENT_SENT = "CONFERENCE_MEASUREMENT_SENT";
        var CONFERENCE_MEASUREMENT_SENT_ERROR = "CONFERENCE_MEASUREMENT_SENT_ERROR";

        var TYPE = 'type';
        var BLOOD_PRESSURE = "BLOOD_PRESSURE";
        var SATURATION = "SATURATION";

        var typeToConfigurationDict = {
            SATURATION: {
                name: 'saturation',
                listener: saturationListener,
                template: $templateCache.get('deviceListeners/measurementTemplates/saturation.html'),
                values: ['saturation', 'pulse']
            },
            BLOOD_PRESSURE: {
                name: 'blood pressure',
                listener: bloodPressureListener,
                template: $templateCache.get('deviceListeners/measurementTemplates/bloodPressure.html'),
                values: ['systolic', 'diastolic', 'meanArterialPressure', 'pulse']
            }
        };

        var clearObject = function(obj, ignoredKeys) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop) &&
                    ignoredKeys.indexOf(prop) === -1) {
                    delete obj[prop];
                }
            }
        };

        var sendMeasurements = function(type, model, valuesToSend, videoConference) {

            var onSuccess = function() {
                clearObject(model, ['state', 'header', 'message', 'info']);
                model.info = CONFERENCE_MEASUREMENT_SENT;
            };

            var onError = function() {
                clearObject(model, ['state', 'header', 'message', 'info']);
                model.info = CONFERENCE_MEASUREMENT_SENT_ERROR;
            };

            var measurementData = {
                type: type,
                deviceId: model.deviceId,
                measurement: {},
                date: new Date().toISOString()
            };

            valuesToSend.forEach(function(value) {
                measurementData.measurement[value] = model[value];
            });

            console.log("Measurement to send:" + JSON.stringify(measurementData));
            var measurementFromPatientUrl = videoConference.measurementFromPatientUrl;
            videoService.sendMeasurementFromPatient(measurementFromPatientUrl, measurementData,
                                                    onSuccess, onError);
        };

        var handlePendingMeasurement = function(videoConference, model, pendingMeasurement) {

            model.template = "";
            nativeService.removeDeviceListeners();

            var configuration = typeToConfigurationDict[pendingMeasurement];

            if (configuration !== undefined) {

                var measurementType = configuration.name;
                var valuesToSend = configuration.values;

                var allValuesEntered = function() {
                    return valuesToSend.reduce(function(previous, value) {
                        var current = model[value] !== undefined;
                        return current && previous;
                    }, true);
                };

                var tryToSendMeasurements = function(newValue, oldValue) {
                    if (newValue) {
                        nativeService.removeDeviceListeners();
                        sendMeasurements(pendingMeasurement, model, valuesToSend, videoConference);
                    }
                };

                $rootScope.$watch(allValuesEntered, tryToSendMeasurements);

                model.template = configuration.template;

                var eventListener = configuration.listener.create(model);
                var nativeEventCallback  = function(message) {
                    if (message.measurementType !== measurementType) {
                        return;
                    }
                    eventListener(message.event);
                };
                nativeService.subscribeToMultipleMessages('deviceMeasurementResponse', nativeEventCallback);
                nativeService.addDeviceListener(measurementType);
            }
        };

        var checkForPendingMeasurements = function(videoConference, model,
                                                   currentPendingMeasurement) {

            var onSuccess = function(pendingMeasurement) {

                var newPendingMeasurement;

                var timeoutWrapper = function() {
                    checkForPendingMeasurements(videoConference, model, newPendingMeasurement);
                };

                if (pendingMeasurement !== undefined &&
                    pendingMeasurement.hasOwnProperty(TYPE)) {
                    newPendingMeasurement = pendingMeasurement.type;
                }

                console.log("old: " + currentPendingMeasurement);
                console.log("new: " + newPendingMeasurement);

                if (newPendingMeasurement !== currentPendingMeasurement) {
                    handlePendingMeasurement(videoConference, model, newPendingMeasurement);
                }

                setTimeout(timeoutWrapper, 3000);

            };

            if (model.state == 'joined') {
                var pendingMeasurementUrl = videoConference.pendingMeasurementUrl;
                videoService.pollForPendingMeasurement(pendingMeasurementUrl, onSuccess);
            } else {
                console.log("Stop: pollForPendingMeasurement");
            }
        };

        var endConference = function(videoConference) {
            console.log("Conference ended");

            nativeService.unsubscribeAll("videoConferenceResponse");
            nativeService.removeDeviceListeners();

            $location.path('/menu');
        };

        var handleStatusEvent = function(videoConference, model, event) {
            var status = event.status;
            var statusType = status.type;
            var message = status.message;

            var handleInfoEvent = function(videoConference, model, message) {
                model.message = message;
                switch (message) {
                case CONFERENCE_STARTED:
                    model.state = "joined";
                    checkForPendingMeasurements(videoConference, model,
                                                undefined);
                    break;
                case CONFERENCE_ENDED:
                    model.state = "disconnected";
                    endConference(videoConference);
                    break;
                default:
                    console.log("Unknown info event: " + event);
                    break;
                }
            };

            var handleErrorEvent = function(videoConference, model, message) {
                model.state = "disconnected";
                switch (message) {
                case CONFERENCE_ERROR:
                    endConference(videoConference);
                    break;
                default:
                    console.log("Unknown error event: " + event);
                    break;
                }
            };

            switch (statusType) {
            case constants.INFO:
                handleInfoEvent(videoConference, model, message);
                break;
            case constants.ERROR:
                handleErrorEvent(videoConference, model, message);
                break;
            default:
                console.log("Unknown status type: " + type);
                break;
            }
        };

        var eventListener = function(videoConference, model, event) {
            var eventType = event.type;

            switch (eventType) {
            case constants.STATUS:
                handleStatusEvent(videoConference, model, event);
                break;

            default:
                console.log("Unknown event type:" + eventType);
                break;
            }
        };

        var setup = function(videoConference, model) {
            var nativeEventCallback = function(message) {
                $timeout(function() {
                    eventListener(videoConference, model, message.event);
                });
            };
            nativeService.subscribeToMultipleMessages("videoConferenceResponse", nativeEventCallback);
        };

        return {
            setup: setup
        };

    });

}());
